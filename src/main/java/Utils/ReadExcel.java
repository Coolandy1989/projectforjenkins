package Utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

public class ReadExcel {
	
	@Test
	public static String[][] readexceldata (String excelfilename) throws IOException{
		
		//Go to the path
		XSSFWorkbook wbook = new XSSFWorkbook("./Data/"+excelfilename+".xlsx");
				
		//Navigate to the sheet
	    XSSFSheet sheet = wbook.getSheet("Sheet1");
		
		//Count number of rows
	    int rowcount = sheet.getLastRowNum();
		
		//Count number of columns
	    
	   int cellcount = sheet.getRow(0).getLastCellNum();
	    
		//Read data from Excel
	   
	   String[][] data = new String[rowcount][cellcount];
	   
	   for (int i = 1; i <= rowcount; i++) {
		
		   XSSFRow row = sheet.getRow(i);
		   
		   for (int j = 0; j < cellcount; j++) {
			   
			   XSSFCell cell = row.getCell(j);
			   data[i-1][j] = cell.getStringCellValue();
			   System.out.println(data);
			
		}
		   
	}
		
		// Close the excel
		
	   wbook.close();
	return data;
	   
	   
	}

}
