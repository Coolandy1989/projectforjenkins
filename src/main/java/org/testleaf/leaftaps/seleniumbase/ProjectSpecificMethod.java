package org.testleaf.leaftaps.seleniumbase;

import java.io.IOException;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import Utils.ReadExcel;

public class ProjectSpecificMethod
{
	public ChromeDriver driver;
	public String excelfilename;
	@Parameters({"url","username","password"})
	@BeforeMethod
	public void login(String url,String username, String password) 
	{
	   System.setProperty("webdriver.chrome.driver", "./drivers/Chromedriver/chromedriver.exe");
		
		driver = new ChromeDriver();	
		driver.get(url);
		
		driver.manage().window().maximize();
		
		driver.findElementById("username").sendKeys(username);
		
		driver.findElementById("password").sendKeys(password);
		
		driver.findElementByClassName("decorativeSubmit").click();
		
		driver.findElementByLinkText("CRM/SFA").click();
	
	
	}
	 @DataProvider(name="inputdata")
	    public  String[][] datainput() throws IOException{
	    	/*
	    	String[][] data = new String[2][3];
	    	
	    	data[0][0] = "Infosys";
	    	data[0][1] = "Dinesh";
	    	data[0][2] = "Anderson";
	    	
	    	data[1][0] = "Infosys";
	    	data[1][1] = "MS";
	    	data[1][2] = "Dhoni";
	    	*/
	   	
			return ReadExcel.readexceldata(excelfilename);
	 }
	
	@AfterMethod
	public void afterMethod() {
		System.out.println("After method");
		driver.close();	
	}
	
	
	@AfterClass
	public void afterclass() {
		System.out.println("After class");
		
	}
	@AfterTest
	public void afterTest() {
		System.out.println("After Test");
		
	}
	@AfterSuite
	public void afterSuite() {
		System.out.println("After Suite");
		
	}
	
	
	
}
