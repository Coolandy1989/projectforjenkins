package ClassCreate;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLead {
	public ChromeDriver driver;
	
	@Given("Launch the Browser")
	public void launchTheBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/Chromedriver/chromedriver.exe");
		driver = new ChromeDriver();			
	    
	}

	@Given("Maximise the Browser")
	public void maximiseTheBrowser() {
	   driver.manage().window().maximize();
	}

	@Given("Set the time outs")
	public void setTheTimeOuts() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Given("Load the URL")
	public void loadTheURL() {
		driver.get("http://leaftaps.com/opentaps");
	}

	@Given("Enter the Username as (.*)")
	public void enterTheUsername(String Username) {
		driver.findElementById("username").sendKeys(Username);
	}

	@Given("Enter the password as (.*)")
	public void enterThePassword(String Password) {
		driver.findElementById("password").sendKeys(Password);
	}

	@Given("Click on Login")
	public void clickOnLogin() {
		driver.findElementByClassName("decorativeSubmit").click();
	}

	@Given("Click on crmsfa")
	public void clickOnCrmsfa() {
		driver.findElementByLinkText("CRM/SFA").click();
	}

	@Given("Click on Create Lead")
	public void clickOnCreateLead() {
		driver.findElementByLinkText("Create Lead").click();
	}

	@When("Create Lead is clicked enter Company name as (.*)")
	public void createLeadIsClickedEnterCompanyName(String cname) {
		driver.findElementById("createLeadForm_companyName").sendKeys(cname);
	}

	
	@When("Enter first name as (.*)")
	public void enterFirstName(String fname ) {
		driver.findElementById("createLeadForm_firstName").sendKeys(fname);
	}

	@When("Enter Last name as (.*)")
	public void enterLastName(String lname) {
		driver.findElementById("createLeadForm_lastName").sendKeys(lname);
	}

	@When("Click create lead button")
	public void clickCreateLeadButton() {
		driver.findElementByXPath("//input[@value='Create Lead']").click();
	}

	@When("Verify the lead created")
	public void verifyTheLeadCreated() {
	    System.out.println("Lead is verified");
	}

	@Then("Lead should be created successfully")
	public void leadShouldBeCreatedSuccessfully() {
	    System.out.println("Lead is created Succuessfully");
	}


}
