Feature: Create Lead

Background: 
Given Launch the Browser
And Maximise the Browser
And Set the time outs
And Load the URL


Scenario Outline: Create lead in Leaf Taps
Given Enter the Username as DemoSalesManager
And Enter the password as crmsfa
And Click on Login
And Click on crmsfa
And Click on Create Lead
When Create Lead is clicked enter Company name as <companyname>
And  Enter first name as <firstname>
And  Enter Last name as <lastname>
And  Click create lead button
And  Verify the lead created
Then Lead should be created successfully

Examples:
|companyname|firstname|lastname|
|Infosys|Dinesh|Anderson|
|Seven|MS|Dhoni|
